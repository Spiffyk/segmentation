package cz.spiffyk.segmentation.gui.components;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

public final class HistogramViewer extends JPanel {

    private static final int MARKINGS_X_MARGIN = 4;
    private static final int MARKINGS_Y_MARGIN = 4;
    private static final int PRIMARY_MARKINGS_LENGTH = 32;

    private transient Supplier<long[]> originalHistogramSupplier = null;
    private transient Supplier<long[]> histogramSupplier = null;
    private transient Supplier<Set<Integer>> thresholdsSupplier = null;

    private final transient Set<Runnable> thresholdChangeListeners = new HashSet<>();

    private Color borderColor = Color.BLACK;
    private Color histogramColor = Color.DARK_GRAY;
    private Color originalHistogramColor = new Color(1f, 0f, 0f, 0.2f);
    private Color markingsColor = Color.RED;
    private Color thresholdsColor = Color.GREEN;
    private Color cursorColor = Color.BLACK;

    private int cursorValue = -1;
    private int markingsCount = 7;

    private final Font markingsFont = new Font(Font.SANS_SERIF, Font.BOLD, 14);
    private final Font positionValueFont = new Font(Font.SANS_SERIF, Font.PLAIN, 12);

    public HistogramViewer() {
        addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                synchronized (HistogramViewer.this) {
                    if (histogramSupplier == null) {
                        return;
                    }
                    final var histogram = histogramSupplier.get();
                    if (histogram == null || histogram.length < 1) {
                        return;
                    }

                    final int width = getWidth();

                    final double bandWidth = (double) width / histogram.length;
                    final double value = e.getX() / bandWidth;
                    cursorValue = (int) Math.round(value);

                    setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
                    if (thresholdsSupplier != null) {
                        final var thresholds = thresholdsSupplier.get();
                        if (thresholds != null && thresholds.contains(cursorValue)) {
                            setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                        }
                    }

                    repaint();
                }
            }
        });

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseExited(MouseEvent e) {
                synchronized (HistogramViewer.this) {
                    cursorValue = -1;
                    repaint();
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                synchronized (HistogramViewer.this) {
                    if (thresholdsSupplier == null || cursorValue < 0) {
                        return;
                    }
                    final var thresholds = thresholdsSupplier.get();
                    if (thresholds == null) {
                        return;
                    }

                    if (e.getButton() == MouseEvent.BUTTON1) {
                        thresholds.add(cursorValue);
                    } else if (e.getButton() == MouseEvent.BUTTON3) {
                        thresholds.remove(cursorValue);
                    }
                    thresholdChangeListeners.forEach(Runnable::run);
                    repaint();
                }
            }
        });
    }

    @Override
    public synchronized void paint(Graphics g) {
        super.paint(g);
        final Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);

        if (histogramSupplier == null) {
            return;
        }
        final var histogram = histogramSupplier.get();
        if (histogram == null || histogram.length < 1) {
            return;
        }

        long[] originalHistogram = null;
        if (originalHistogramSupplier != null) {
            originalHistogram = originalHistogramSupplier.get();
        }

        paintHistogram(g2, histogramColor, histogram);
        if (originalHistogram != null) {
            paintHistogram(g2, originalHistogramColor, originalHistogram);
        }
        paintMarkings(g2, histogram);
        paintThresholds(g2, histogram);
        paintCursor(g2, histogram);
        paintBorder(g2);
    }

    private void paintHistogram(Graphics2D g2, Color color, long[] histogram) {
        final int width = getWidth();
        final int height = getHeight();

        final long maxLong = Arrays.stream(histogram).max()
                .orElseThrow(() -> new IllegalStateException("There was no max value???"));
        final double maxDouble = (double) maxLong;
        final double bandWidth = (double) width / histogram.length;
        final int bandWidthInt = (int) Math.ceil(bandWidth);
        g2.setColor(color);
        for (int i = 0; i < histogram.length; i++) {
            final double bandHeight = histogram[i] / maxDouble;
            final int bandHeightInt = (int) (bandHeight * height);
            final int x = (int) (i * bandWidth);
            g2.fillRect(x, height - bandHeightInt, bandWidthInt, bandHeightInt);
        }
    }

    private void paintMarkings(Graphics2D g2, long[] histogram) {
        final int width = getWidth();
        final int height = getHeight();

        final double markingsHistogramSpaceDistance = ((double) histogram.length / (markingsCount - 1));
        final double bandWidth = (double) width / histogram.length;

        if (bandWidth > 3) {
            g2.setColor(new Color(
                    histogramColor.getRed(),
                    histogramColor.getGreen(),
                    histogramColor.getBlue(),
                    (int) (histogramColor.getAlpha() * 0.2)
            ));
            for (int i = 0; i < histogram.length; i++) {
                final int x = (int) (i * bandWidth);
                g2.fillRect(x, 0, 1, height);
            }
        }

        g2.setFont(markingsFont);
        final FontMetrics metrics = g2.getFontMetrics(markingsFont);
        for (int i = 0; i < markingsCount; i++) {
            final int x = (int) (i * markingsHistogramSpaceDistance * bandWidth);
            g2.fillRect(x, height - PRIMARY_MARKINGS_LENGTH, 1, PRIMARY_MARKINGS_LENGTH);

            final String markingText = Integer.toString((int) (i * markingsHistogramSpaceDistance));
            final int textWidth = metrics.stringWidth(markingText);
            final int textX = (x + MARKINGS_X_MARGIN + textWidth > width) ?
                    x - MARKINGS_X_MARGIN - textWidth :
                    x + MARKINGS_X_MARGIN;
            final int textY = height - PRIMARY_MARKINGS_LENGTH - MARKINGS_Y_MARGIN;
            g2.setColor(getBackground());
            g2.fillRect(textX, textY, textWidth, metrics.getHeight());
            g2.setColor(markingsColor);
            g2.drawString(markingText, textX, textY + metrics.getAscent());
        }
    }

    private void paintThresholds(Graphics2D g2, long[] histogram) {
        final int width = getWidth();
        final int height = getHeight();

        if (thresholdsSupplier == null) {
            return;
        }
        final var thresholds = thresholdsSupplier.get();
        if (thresholds == null) {
            return;
        }

        final double bandWidth = (double) width / histogram.length;
        g2.setFont(markingsFont);
        final FontMetrics metrics = g2.getFontMetrics(markingsFont);
        for (final var threshold : thresholds) {
            final int x = (int) (threshold * bandWidth);
            g2.setColor(thresholdsColor);
            g2.fillRect(x, 0, 1, height);

            final String markingText = threshold.toString();
            final int textWidth = metrics.stringWidth(markingText);
            final int textX = (x + MARKINGS_X_MARGIN + textWidth > width) ?
                    x - MARKINGS_X_MARGIN - textWidth :
                    x + MARKINGS_X_MARGIN;

            g2.setColor(histogramColor);
            g2.fillRect(textX, MARKINGS_Y_MARGIN, textWidth, metrics.getHeight());
            g2.setColor(thresholdsColor);
            g2.drawString(markingText, textX, MARKINGS_Y_MARGIN + metrics.getAscent());
        }
    }

    private void paintCursor(Graphics2D g2, long[] histogram) {
        final int width = getWidth();
        final int height = getHeight();

        if (cursorValue < 0 || cursorValue >= histogram.length) {
            return;
        }

        final double bandWidth = (double) width / histogram.length;
        g2.setColor(cursorColor);
        g2.setFont(markingsFont);

        final int x = (int) (cursorValue * bandWidth);
        g2.fillRect(x, 0, 1, height);

        final FontMetrics markingMetrics = g2.getFontMetrics(markingsFont);
        final int markingAscent = markingMetrics.getAscent();
        final String markingText = Integer.toString(cursorValue);
        final int markingWidth = markingMetrics.stringWidth(markingText);
        final int markingX = (x + MARKINGS_X_MARGIN + markingWidth > width) ?
                x - MARKINGS_X_MARGIN - markingWidth :
                x + MARKINGS_X_MARGIN;

        g2.setColor(getBackground());
        g2.fillRect(markingX, MARKINGS_Y_MARGIN, markingWidth, markingMetrics.getHeight());
        g2.setColor(cursorColor);
        g2.drawString(markingText, markingX, markingAscent + MARKINGS_Y_MARGIN);

        final FontMetrics valueMetrics = g2.getFontMetrics(positionValueFont);
        final int valueAscent = valueMetrics.getAscent();
        final String valueText = Long.toString(histogram[cursorValue]);
        final int valueWidth = valueMetrics.stringWidth(valueText);
        final int valueX = (x + MARKINGS_X_MARGIN + valueWidth > width) ?
                x - MARKINGS_X_MARGIN - valueWidth :
                x + MARKINGS_X_MARGIN;
        final int valueY = 2 * MARKINGS_Y_MARGIN + markingAscent;

        g2.setColor(getBackground());
        g2.fillRect(valueX, valueY, valueWidth, valueMetrics.getHeight());
        g2.setColor(cursorColor);
        g2.setFont(positionValueFont);
        g2.drawString(valueText, valueX, valueAscent + valueY);
    }

    private void paintBorder(Graphics2D g2) {
        g2.setColor(borderColor);
        g2.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
    }

    public synchronized void setHistogramSupplier(Supplier<long[]> histogramSupplier) {
        this.histogramSupplier = histogramSupplier;
        repaint();
    }

    public synchronized void setOriginalHistogramSupplier(Supplier<long[]> supplier) {
        this.originalHistogramSupplier = supplier;
        repaint();
    }

    public synchronized void setThresholdsSupplier(Supplier<Set<Integer>> thresholdsSupplier) {
        this.thresholdsSupplier = thresholdsSupplier;
        repaint();
    }

    public synchronized Color getBorderColor() {
        return borderColor;
    }

    public synchronized void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
        repaint();
    }

    public synchronized Color getHistogramColor() {
        return histogramColor;
    }

    public synchronized void setHistogramColor(Color histogramColor) {
        this.histogramColor = histogramColor;
        repaint();
    }

    public synchronized Color getMarkingsColor() {
        return markingsColor;
    }

    public synchronized void setMarkingsColor(Color markingsColor) {
        this.markingsColor = markingsColor;
        repaint();
    }

    public synchronized Color getThresholdsColor() {
        return thresholdsColor;
    }

    public synchronized void setThresholdsColor(Color thresholdsColor) {
        this.thresholdsColor = thresholdsColor;
    }

    public Color getCursorColor() {
        return cursorColor;
    }

    public void setCursorColor(Color cursorColor) {
        this.cursorColor = cursorColor;
    }

    public synchronized int getMarkingsCount() {
        return markingsCount;
    }

    public synchronized void setMarkingsCount(int markingsCount) {
        this.markingsCount = markingsCount;
        repaint();
    }

    public synchronized Font getMarkingsFont() {
        return markingsFont;
    }

    public synchronized void addThesholdChangeListener(Runnable runnable) {
        thresholdChangeListeners.add(runnable);
    }
}
