package cz.spiffyk.segmentation.gui.components;

import cz.spiffyk.segmentation.ImageProcessingException;
import cz.spiffyk.segmentation.gui.engine.ImageMode;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public final class ImageViewer extends JPanel {

    private ImageMode.ImageSupplier imageSupplier = null;

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        final Graphics2D g2 = (Graphics2D) g;

        if (imageSupplier == null) {
            return;
        }

        final BufferedImage image;
        try {
            image = imageSupplier.get();
        } catch (ImageProcessingException e) {
            e.printStackTrace();
            return;
        }

        if (image == null) {
            return;
        }

        final int width = getWidth();
        final int height = getHeight();
        final int imgWidth = image.getWidth();
        final int imgHeight = image.getHeight();

        double scale = (double) width / imgWidth;
        if (imgHeight * scale > height) {
            scale = (double) height / imgHeight;
        }

        final int imgWidthScaled = (int) (imgWidth * scale);
        final int imgHeightScaled = (int) (imgHeight * scale);

        final int x = (width - imgWidthScaled) / 2;
        final int y = (height - imgHeightScaled) / 2;

        g2.translate(x, y);
        g2.scale(scale, scale);
        g2.drawImage(image, null, 0, 0);
    }

    public void setImageSupplier(ImageMode.ImageSupplier imageSupplier) {
        this.imageSupplier = imageSupplier;
        repaint();
    }
}
