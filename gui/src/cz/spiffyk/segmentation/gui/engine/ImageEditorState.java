package cz.spiffyk.segmentation.gui.engine;

import cz.spiffyk.segmentation.MeltingDirection;

import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.Set;

class ImageEditorState {

    final BufferedImage inputImage;
    long[] originalHistogram;
    long[] inputHistogram;
    BufferedImage outputImage = null;

    int autoThresholdDepth = 1;
    boolean includeMax = false;
    int cooccurrenceIterations = 32;
    int meltingStart = 127;
    int meltingSteps = 32;
    MeltingDirection meltingDirection = MeltingDirection.BIDIRECTIONAL;
    int filterRadius = 1;

    ImageMode outputMode = null;
    final Set<Integer> thresholds = new HashSet<>();

    public ImageEditorState(BufferedImage inputImage, long[] inputHistogram) {
        this.inputImage = inputImage;
        this.originalHistogram = inputHistogram;
        this.inputHistogram = inputHistogram;
    }

}
