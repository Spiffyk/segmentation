package cz.spiffyk.segmentation.gui.engine;

import cz.spiffyk.segmentation.ImageProcessingException;

import java.awt.image.BufferedImage;

public enum ImageMode {
    INPUT(ImageEditorEngine::input),
    THRESHOLDING(ImageEditorEngine::thresholded),
    COOCCURRENCE_MATRIX(ImageEditorEngine::cooccurrence),
    MELTING(ImageEditorEngine::melting),
    ;

    public final ImageFunction imageFunction;

    ImageMode(ImageFunction imageFunction) {
        this.imageFunction = imageFunction;
    }


    @FunctionalInterface
    public interface ImageFunction {
        BufferedImage apply(ImageEditorEngine engine) throws ImageProcessingException;
    }

    @FunctionalInterface
    public interface ImageSupplier {
        BufferedImage get() throws ImageProcessingException;
    }
}
