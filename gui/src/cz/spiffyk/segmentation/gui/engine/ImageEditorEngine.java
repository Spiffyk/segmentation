package cz.spiffyk.segmentation.gui.engine;

import cz.spiffyk.segmentation.ImageProcessingException;
import cz.spiffyk.segmentation.ImageProcessingService;
import cz.spiffyk.segmentation.MeltingDirection;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 *
 */
public class ImageEditorEngine {

    private static final int DEFAULT_HISTORY_LIMIT = 20;

    private static final ImageProcessingService imageProcessingService =
            ServiceLoader.load(ImageProcessingService.class)
                    .findFirst().orElseThrow(() -> new NoSuchElementException("SegmentationService"));

    private int historyLimit;
    private final LinkedList<ImageEditorState> history = new LinkedList<>();
    private ImageEditorState currentState;


    public ImageEditorEngine(BufferedImage inputImage) throws ImageProcessingException {
        this(inputImage, DEFAULT_HISTORY_LIMIT);
    }

    public ImageEditorEngine(BufferedImage inputImage, int historyLimit) throws ImageProcessingException {
        this.historyLimit = historyLimit;
        currentState = new ImageEditorState(inputImage, imageProcessingService.histogram(inputImage));
        history.add(currentState);
    }


    public static ImageEditorEngine load(File file) throws ImageProcessingException, IOException {
        return load(file, DEFAULT_HISTORY_LIMIT);
    }

    public static ImageEditorEngine load(File file, int historyLimit) throws ImageProcessingException, IOException {
        final BufferedImage image = ImageIO.read(file);
        imageProcessingService.checkSupport(image);
        return new ImageEditorEngine(image, historyLimit);
    }


    public void commit() throws ImageProcessingException {
        final var outputImage = currentState.outputImage;
        currentState = new ImageEditorState(outputImage, imageProcessingService.histogram(outputImage));
        history.add(currentState);

        while (history.size() > historyLimit) {
            history.removeFirst();
        }
    }

    public void invalidateOutput() {
        currentState.outputImage = null;
    }

    public void undo() {
        if (history.size() <= 1) {
            currentState.outputMode = ImageMode.INPUT;
            return;
        }

        history.removeLast();
        currentState = history.getLast();
    }


    public BufferedImage input() {
        if (currentState.outputImage == null || currentState.outputMode != ImageMode.INPUT) {
            currentState.outputImage = currentState.inputImage;
            currentState.outputMode = ImageMode.INPUT;
        }

        return currentState.outputImage;
    }

    public BufferedImage thresholded() throws ImageProcessingException {
        if (currentState.outputImage == null || currentState.outputMode != ImageMode.THRESHOLDING) {
            currentState.outputMode = ImageMode.THRESHOLDING;
            currentState.outputImage = imageProcessingService.threshold(currentState.inputImage, currentState.thresholds);
        }

        return currentState.outputImage;
    }

    public BufferedImage cooccurrence() throws ImageProcessingException {
        if (currentState.outputImage == null || currentState.outputMode != ImageMode.COOCCURRENCE_MATRIX) {
            currentState.outputMode = ImageMode.COOCCURRENCE_MATRIX;
            currentState.outputImage = imageProcessingService.cooccurrence(currentState.inputImage, currentState.cooccurrenceIterations);
        }

        return currentState.outputImage;
    }

    public BufferedImage melting() throws ImageProcessingException {
        if (currentState.outputImage == null || currentState.outputMode != ImageMode.MELTING) {
            currentState.outputMode = ImageMode.MELTING;
            currentState.outputImage = imageProcessingService.melt(
                    currentState.inputImage,
                    currentState.meltingStart,
                    currentState.meltingSteps,
                    currentState.meltingDirection);
        }

        return currentState.outputImage;
    }


    public ImageMode lastImageMode() {
        return (currentState.outputMode == null) ? ImageMode.INPUT : currentState.outputMode;
    }


    public void regenerateHistogram() throws ImageProcessingException {
        currentState.inputHistogram = imageProcessingService.histogram(currentState.inputImage);
    }

    public void linearFilterHistogram() {
        currentState.inputHistogram = imageProcessingService.linearFilterHistogram(
                currentState.inputHistogram, currentState.filterRadius);
    }

    public void dilationFilterHistogram() {
        currentState.inputHistogram = imageProcessingService.dilationFilterHistogram(
                currentState.inputHistogram, currentState.filterRadius);
    }

    public void erosionFilterHistogram() {
        currentState.inputHistogram = imageProcessingService.erosionFilterHistogram(
                currentState.inputHistogram, currentState.filterRadius);
    }


    public void defaultThresholds() {
        currentState.thresholds.clear();
        currentState.thresholds.add(0);
        currentState.thresholds.add(127);
        currentState.thresholds.add(255);
        invalidateOutput();
    }

    public void autoThresholdsBimodal() {
        currentState.thresholds.clear();
        currentState.thresholds.addAll(imageProcessingService.bimodalAutoThreshold(
                currentState.inputHistogram, currentState.includeMax));
        invalidateOutput();
    }

    public void autoThresholdsMultimodal() {
        currentState.thresholds.clear();
        currentState.thresholds.addAll(imageProcessingService.multimodalAutoThreshold(
                currentState.inputHistogram, currentState.autoThresholdDepth));
        invalidateOutput();
    }

    public void clearThresholds() {
        currentState.thresholds.clear();
        invalidateOutput();
    }


    public long[] histogram() {
        return currentState.inputHistogram;
    }

    public long[] originalHistogram() {
        return currentState.originalHistogram;
    }

    public Set<Integer> thresholds() {
        return currentState.thresholds;
    }

    public int getAutoThresholdDepth() {
        return currentState.autoThresholdDepth;
    }

    public void setAutoThresholdDepth(int depth) {
        currentState.autoThresholdDepth = depth;
    }

    public int getCooccurrenceIterations() {
        return currentState.cooccurrenceIterations;
    }

    public void setCooccurrenceIterations(int iterations) {
        currentState.cooccurrenceIterations = iterations;
    }

    public int getMeltingStart() {
        return currentState.meltingStart;
    }

    public void setMeltingStart(int start) {
        currentState.meltingStart = start;
    }

    public int getMeltingSteps() {
        return currentState.meltingSteps;
    }

    public void setMeltingSteps(int steps) {
        currentState.meltingSteps = steps;
    }

    public MeltingDirection getMeltingDirection() {
        return currentState.meltingDirection;
    }

    public void setMeltingDirection(MeltingDirection direction) {
        Objects.requireNonNull(direction, "Direction must not be null!");
        currentState.meltingDirection = direction;
    }

    public boolean getIncludeMax() {
        return currentState.includeMax;
    }

    public void setIncludeMax(boolean includeMax) {
        currentState.includeMax = includeMax;
    }

    public int getFilterRadius() {
        return currentState.filterRadius;
    }

    public void setFilterRadius(int radius) {
        currentState.filterRadius = radius;
    }

    public int getHistoryLimit() {
        return historyLimit;
    }

    public void setHistoryLimit(int historyLimit) {
        this.historyLimit = historyLimit;
    }
}
