package cz.spiffyk.segmentation.gui;

import cz.spiffyk.segmentation.ImageProcessingException;
import cz.spiffyk.segmentation.MeltingDirection;
import cz.spiffyk.segmentation.gui.components.HistogramViewer;
import cz.spiffyk.segmentation.gui.components.ImageViewer;
import cz.spiffyk.segmentation.gui.engine.ImageEditorEngine;
import cz.spiffyk.segmentation.gui.engine.ImageMode;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@SuppressWarnings("java:S110")
public final class MainAppWindow extends JFrame {

    private static final String PROGRAM_TITLE = "Segmentation";
    private static final int TOOLBAR_WIDTH = 280;
    private static final int HISTOGRAM_HEIGHT = 250;

    private static final Dimension PANEL_MARGIN = new Dimension(12, 12);
    private static final Dimension CONTROL_MARGIN = new Dimension(4, 4);

    private ImageMode mode;

    private transient ImageEditorEngine engine;

    private final JFileChooser openImageChooser = new JFileChooser();

    private final transient ButtonModel includeMaxModel;

    private final transient SpinnerModel cooccurrenceIterationsModel = new SpinnerNumberModel(0, 0, 255, 1);
    private final transient SpinnerModel autoThresholdsDepthModel = new SpinnerNumberModel(1, 1, 32, 1);
    private final transient SpinnerModel meltingStartModel = new SpinnerNumberModel(0, 0, 255, 1);
    private final transient SpinnerModel meltingStepsModel = new SpinnerNumberModel(0, 0, 255, 1);
    private final transient SpinnerModel filterRadiusModel = new SpinnerNumberModel(1, 1, 255, 1);

    private final transient ButtonGroup imageModeGroup = new ButtonGroup();
    private final transient Map<ImageMode, JRadioButton> imageModeButtonMap = new EnumMap<>(ImageMode.class);

    private final transient ButtonGroup meltingDirectionGroup = new ButtonGroup();
    private final transient Map<MeltingDirection, JRadioButton> meltingDirectionButtonMap = new EnumMap<>(MeltingDirection.class);

    {
        final var suffixes = ImageIO.getReaderFileSuffixes();
        final var imageFilterDescription = String.format("Image (%s)", Arrays.stream(suffixes)
                .map(suffix -> "." + suffix)
                .collect(Collectors.joining(", ")));
        openImageChooser.setFileFilter(
                new FileNameExtensionFilter(imageFilterDescription, suffixes));
    }

    private final JFileChooser saveImageChooser = new JFileChooser();

    {
        saveImageChooser.setFileFilter(new FileNameExtensionFilter("Bitmap image (bmp)", "bmp"));
    }

    private final HistogramViewer histogramViewer = new HistogramViewer();

    private final ImageViewer imageViewer = new ImageViewer();

    {
        imageViewer.setBackground(Color.BLACK);
    }

    private final JPanel toolbar = new JPanel();

    {
        toolbar.setLayout(new BoxLayout(toolbar, BoxLayout.Y_AXIS));
        toolbar.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));

        final JPanel filePanel = new JPanel();
        filePanel.setLayout(new BoxLayout(filePanel, BoxLayout.Y_AXIS));
        filePanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        filePanel.setBorder(BorderFactory.createTitledBorder("File"));
        {
            final JButton openImageButton = new JButton("Open image...");
            openImageButton.setToolTipText("Open an image file for processing");
            openImageButton.setAlignmentX(Component.CENTER_ALIGNMENT);
            openImageButton.addActionListener(event -> onOpenImageClicked());
            filePanel.add(openImageButton);

            filePanel.add(Box.createRigidArea(CONTROL_MARGIN));

            final JButton saveImageButton = new JButton("Save image...");
            saveImageButton.setToolTipText("Save the currently displayed image");
            saveImageButton.setAlignmentX(Component.CENTER_ALIGNMENT);
            saveImageButton.addActionListener(event -> onSaveImageClicked());
            filePanel.add(saveImageButton);
        }
        toolbar.add(filePanel);

        toolbar.add(Box.createRigidArea(PANEL_MARGIN));

        final JPanel imageModePanel = new JPanel();
        imageModePanel.setLayout(new BoxLayout(imageModePanel, BoxLayout.Y_AXIS));
        imageModePanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        imageModePanel.setBorder(BorderFactory.createTitledBorder("Image mode"));
        {
            final JRadioButton inputRadio = new JRadioButton("Input");
            inputRadio.setAlignmentX(Component.LEFT_ALIGNMENT);
            inputRadio.setToolTipText("Shows the input image as-is");
            inputRadio.addActionListener(event -> setMode(ImageMode.INPUT));
            imageModePanel.add(inputRadio);
            imageModeGroup.add(inputRadio);
            imageModeButtonMap.put(ImageMode.INPUT, inputRadio);

            final JRadioButton thresholdedRadio = new JRadioButton("Thresholded");
            thresholdedRadio.setAlignmentX(Component.LEFT_ALIGNMENT);
            thresholdedRadio.setToolTipText("Shows the thresholded image");
            thresholdedRadio.addActionListener(event -> setMode(ImageMode.THRESHOLDING));
            imageModePanel.add(thresholdedRadio);
            imageModeGroup.add(thresholdedRadio);
            imageModeButtonMap.put(ImageMode.THRESHOLDING, thresholdedRadio);

            final JPanel cooccurrencePanel = new JPanel();
            cooccurrencePanel.setLayout(new BoxLayout(cooccurrencePanel, BoxLayout.X_AXIS));
            cooccurrencePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
            {
                final JRadioButton cooccurrenceRadio = new JRadioButton("Co-occurrence Matrix");
                cooccurrenceRadio.setAlignmentY(Component.CENTER_ALIGNMENT);
                cooccurrenceRadio.setToolTipText("Shows the image processed using its adjacency matrix");
                cooccurrenceRadio.addActionListener(event -> setMode(ImageMode.COOCCURRENCE_MATRIX));
                cooccurrencePanel.add(cooccurrenceRadio);
                imageModeGroup.add(cooccurrenceRadio);
                imageModeButtonMap.put(ImageMode.COOCCURRENCE_MATRIX, cooccurrenceRadio);

                cooccurrenceIterationsModel.addChangeListener(event -> {
                    if (engine == null) {
                        return;
                    }

                    if (cooccurrenceIterationsModel.getValue() instanceof Integer) {
                        engine.setCooccurrenceIterations((int) cooccurrenceIterationsModel.getValue());
                        engine.invalidateOutput();
                        repaint();
                    }
                });
                final JSpinner iterationsSpinner = new JSpinner(cooccurrenceIterationsModel);
                iterationsSpinner.setToolTipText("Number of iterations of co-occurrence matrix processing");
                iterationsSpinner.setAlignmentY(Component.CENTER_ALIGNMENT);
                iterationsSpinner.setMaximumSize(new Dimension(Integer.MAX_VALUE, 24));
                cooccurrencePanel.add(iterationsSpinner);
            }
            imageModePanel.add(cooccurrencePanel);

            final JPanel meltingPanel = new JPanel();
            meltingPanel.setLayout(new BoxLayout(meltingPanel, BoxLayout.X_AXIS));
            meltingPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
            {
                final JRadioButton meltingRadio = new JRadioButton("Melting");
                meltingRadio.setAlignmentY(Component.CENTER_ALIGNMENT);
                meltingRadio.setToolTipText("Shows the image processed by brightness melting");
                meltingRadio.addActionListener(event -> setMode(ImageMode.MELTING));
                meltingPanel.add(meltingRadio);
                imageModeGroup.add(meltingRadio);
                imageModeButtonMap.put(ImageMode.MELTING, meltingRadio);

                meltingStartModel.addChangeListener(event -> {
                    if (engine == null) {
                        return;
                    }

                    if (meltingStartModel.getValue() instanceof Integer) {
                        engine.setMeltingStart((int) meltingStartModel.getValue());
                        engine.invalidateOutput();
                        repaint();
                    }
                });
                final JSpinner startSpinner = new JSpinner(meltingStartModel);
                startSpinner.setToolTipText("The starting brightness of brightness melting");
                startSpinner.setAlignmentY(Component.CENTER_ALIGNMENT);
                startSpinner.setMaximumSize(new Dimension(Integer.MAX_VALUE, 24));
                meltingPanel.add(startSpinner);

                meltingPanel.add(Box.createRigidArea(CONTROL_MARGIN));

                meltingStepsModel.addChangeListener(event -> {
                    if (engine != null && meltingStepsModel.getValue() instanceof Integer) {
                        engine.setMeltingSteps((int) meltingStepsModel.getValue());
                        engine.invalidateOutput();
                        repaint();
                    }
                });
                final JSpinner stepsSpinner = new JSpinner(meltingStepsModel);
                stepsSpinner.setToolTipText("The steps of brightness melting");
                stepsSpinner.setAlignmentY(Component.CENTER_ALIGNMENT);
                stepsSpinner.setMaximumSize(new Dimension(Integer.MAX_VALUE, 24));
                meltingPanel.add(stepsSpinner);
            }
            imageModePanel.add(meltingPanel);

            imageModeGroup.setSelected(inputRadio.getModel(), true);

            imageModePanel.add(Box.createRigidArea(PANEL_MARGIN));

            final JButton commitButton = new JButton("Commit");
            commitButton.setAlignmentX(Component.LEFT_ALIGNMENT);
            commitButton.addActionListener(event -> onCommitClicked());
            imageModePanel.add(commitButton);

            imageModePanel.add(Box.createRigidArea(CONTROL_MARGIN));

            final JButton undoButton = new JButton("Undo");
            undoButton.setAlignmentX(Component.LEFT_ALIGNMENT);
            undoButton.addActionListener(event -> onUndoClicked());
            imageModePanel.add(undoButton);
        }
        toolbar.add(imageModePanel);

        toolbar.add(Box.createRigidArea(PANEL_MARGIN));

        final JPanel meltingDirPanel = new JPanel();
        meltingDirPanel.setLayout(new BoxLayout(meltingDirPanel, BoxLayout.Y_AXIS));
        meltingDirPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        meltingDirPanel.setBorder(BorderFactory.createTitledBorder("Melting direction"));
        {
            final JRadioButton bidirRadio = new JRadioButton("Bi-directional");
            bidirRadio.setAlignmentX(Component.LEFT_ALIGNMENT);
            bidirRadio.addActionListener(event -> setMeltingDirection(MeltingDirection.BIDIRECTIONAL));
            meltingDirPanel.add(bidirRadio);
            meltingDirectionGroup.add(bidirRadio);
            meltingDirectionButtonMap.put(MeltingDirection.BIDIRECTIONAL, bidirRadio);

            final JRadioButton brighterToDarkerRadio = new JRadioButton("Brighter to darker");
            brighterToDarkerRadio.setAlignmentX(Component.LEFT_ALIGNMENT);
            brighterToDarkerRadio.addActionListener(event -> setMeltingDirection(MeltingDirection.BRIGHTER_TO_DARKER));
            meltingDirPanel.add(brighterToDarkerRadio);
            meltingDirectionGroup.add(brighterToDarkerRadio);
            meltingDirectionButtonMap.put(MeltingDirection.BRIGHTER_TO_DARKER, brighterToDarkerRadio);

            final JRadioButton darkerToBrighterRadio = new JRadioButton("Darker to brighter");
            darkerToBrighterRadio.setAlignmentX(Component.LEFT_ALIGNMENT);
            darkerToBrighterRadio.addActionListener(event -> setMeltingDirection(MeltingDirection.DARKER_TO_BRIGHTER));
            meltingDirPanel.add(darkerToBrighterRadio);
            meltingDirectionGroup.add(darkerToBrighterRadio);
            meltingDirectionButtonMap.put(MeltingDirection.DARKER_TO_BRIGHTER, darkerToBrighterRadio);

            meltingDirectionGroup.setSelected(bidirRadio.getModel(), true);
        }
        toolbar.add(meltingDirPanel);

        toolbar.add(Box.createRigidArea(PANEL_MARGIN));

        final JPanel histogramPanel = new JPanel();
        histogramPanel.setLayout(new BoxLayout(histogramPanel, BoxLayout.Y_AXIS));
        histogramPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        histogramPanel.setBorder(BorderFactory.createTitledBorder("Histogram filtering"));
        {
            final JButton regenerateButton = new JButton("Reset");
            regenerateButton.setToolTipText("Regenerates the histogram from the input image");
            regenerateButton.setAlignmentX(Component.CENTER_ALIGNMENT);
            regenerateButton.addActionListener(event -> regenerateHistogram());
            histogramPanel.add(regenerateButton);

            histogramPanel.add(Box.createRigidArea(CONTROL_MARGIN));

            final JButton linearFilterButton = new JButton("Average");
            linearFilterButton.setToolTipText("Filters the current histogram using an averaging FIR filter");
            linearFilterButton.setAlignmentX(Component.CENTER_ALIGNMENT);
            linearFilterButton.addActionListener(event -> linearFilterHistogram());
            histogramPanel.add(linearFilterButton);

            histogramPanel.add(Box.createRigidArea(CONTROL_MARGIN));

            final JButton dilationFilterButton = new JButton("Dilate");
            dilationFilterButton.setToolTipText("Filters the current histogram using a dilation filter");
            dilationFilterButton.setAlignmentX(Component.CENTER_ALIGNMENT);
            dilationFilterButton.addActionListener(event -> dilationFilterHistogram());
            histogramPanel.add(dilationFilterButton);

            histogramPanel.add(Box.createRigidArea(CONTROL_MARGIN));

            final JButton erosionFilterButton = new JButton("Erode");
            erosionFilterButton.setToolTipText("Filters the current histogram using a erosion filter");
            erosionFilterButton.setAlignmentX(Component.CENTER_ALIGNMENT);
            erosionFilterButton.addActionListener(event -> erosionFilterHistogram());
            histogramPanel.add(erosionFilterButton);

            histogramPanel.add(Box.createRigidArea(CONTROL_MARGIN));

            filterRadiusModel.addChangeListener(event -> {
                if (engine != null && filterRadiusModel.getValue() instanceof Integer) {
                    engine.setFilterRadius((int) filterRadiusModel.getValue());
                }
            });
            final JSpinner radiusSpinner = new JSpinner(filterRadiusModel);
            radiusSpinner.setToolTipText("The \"radius\" of the filters above. 1 -> 3-point filter; 2 -> 5-point filter etc.");
            radiusSpinner.setAlignmentX(Component.CENTER_ALIGNMENT);
            radiusSpinner.setMaximumSize(new Dimension(Integer.MAX_VALUE, 24));
            histogramPanel.add(radiusSpinner);
        }
        toolbar.add(histogramPanel);

        toolbar.add(Box.createRigidArea(PANEL_MARGIN));

        final JPanel thresholdsPanel = new JPanel();
        thresholdsPanel.setLayout(new BoxLayout(thresholdsPanel, BoxLayout.Y_AXIS));
        thresholdsPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        thresholdsPanel.setBorder(BorderFactory.createTitledBorder("Thresholds"));
        {
            final JButton clearThresholdsButton = new JButton("Remove all");
            clearThresholdsButton.setToolTipText("Removes all defined thresholds");
            clearThresholdsButton.setAlignmentX(Component.LEFT_ALIGNMENT);
            clearThresholdsButton.addActionListener(event -> {
                if (engine != null) {
                    engine.clearThresholds();
                    repaint();
                }
            });
            thresholdsPanel.add(clearThresholdsButton);

            thresholdsPanel.add(Box.createRigidArea(CONTROL_MARGIN));

            final JButton defaultThresholdsButton = new JButton("Default");
            defaultThresholdsButton.setToolTipText("Creates thresholds at brightnesses 0, 127 and 255");
            defaultThresholdsButton.setAlignmentX(Component.LEFT_ALIGNMENT);
            defaultThresholdsButton.addActionListener(event -> defaultThresholds());
            thresholdsPanel.add(defaultThresholdsButton);

            thresholdsPanel.add(Box.createRigidArea(CONTROL_MARGIN));

            final JPanel bimodPanel = new JPanel();
            bimodPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
            bimodPanel.setLayout(new BoxLayout(bimodPanel, BoxLayout.X_AXIS));
            {
                final JButton autoThresholdsButton = new JButton("Bimodal");
                autoThresholdsButton.setToolTipText("Creates thresholds using bimodal histogram generation");
                autoThresholdsButton.setAlignmentY(Component.CENTER_ALIGNMENT);
                autoThresholdsButton.addActionListener(event -> autoThresholdsBimodal());
                bimodPanel.add(autoThresholdsButton);

                final JCheckBox includeMaxCheckbox = new JCheckBox("Maxes");
                includeMaxModel = includeMaxCheckbox.getModel();
                includeMaxCheckbox.setToolTipText("Whether the local maximums in the bimodal histogram should be included among the thresholds.");
                includeMaxCheckbox.setAlignmentX(Component.CENTER_ALIGNMENT);
                includeMaxCheckbox.addActionListener(event -> setIncludeMax(includeMaxCheckbox.isSelected()));
                bimodPanel.add(includeMaxCheckbox);
            }
            thresholdsPanel.add(bimodPanel);

            thresholdsPanel.add(Box.createRigidArea(CONTROL_MARGIN));

            final JPanel multimodPanel = new JPanel();
            multimodPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
            multimodPanel.setLayout(new BoxLayout(multimodPanel, BoxLayout.X_AXIS));
            {
                final JButton autoThresholdsButton = new JButton("Multimodal");
                autoThresholdsButton.setToolTipText("Creates thresholds using multi-modal histogram generation");
                autoThresholdsButton.setAlignmentY(Component.CENTER_ALIGNMENT);
                autoThresholdsButton.addActionListener(event -> autoThresholdsMultimodal());
                multimodPanel.add(autoThresholdsButton);

                autoThresholdsDepthModel.addChangeListener(event -> {
                    if (engine == null) {
                        return;
                    }

                    if (autoThresholdsDepthModel.getValue() instanceof Integer) {
                        engine.setAutoThresholdDepth((int) autoThresholdsDepthModel.getValue());
                        engine.invalidateOutput();
                        repaint();
                    }
                });
                final JSpinner depthSpinner = new JSpinner(autoThresholdsDepthModel);
                depthSpinner.setToolTipText("Automatic thresholds generator recursion depth (1 for bi-modal histogram)");
                depthSpinner.setAlignmentY(Component.CENTER_ALIGNMENT);
                depthSpinner.setMaximumSize(new Dimension(Integer.MAX_VALUE, 24));
                multimodPanel.add(depthSpinner);
            }
            thresholdsPanel.add(multimodPanel);
        }
        toolbar.add(thresholdsPanel);
    }

    private final JSplitPane visualizationPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);

    {
        visualizationPane.setTopComponent(imageViewer);
        visualizationPane.setBottomComponent(histogramViewer);
        visualizationPane.setDividerSize(4);
        visualizationPane.setResizeWeight(0.7);
    }

    private final JSplitPane toolbarAndVisualizationPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);

    {
        toolbarAndVisualizationPane.setLeftComponent(toolbar);
        toolbarAndVisualizationPane.setRightComponent(visualizationPane);
        toolbarAndVisualizationPane.setDividerSize(8);
        toolbarAndVisualizationPane.setResizeWeight(0.1);
    }

    private MainAppWindow() {
        setTitle(PROGRAM_TITLE);
        setLocationRelativeTo(null);
        setSize(1024, 768);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        add(toolbarAndVisualizationPane);

        setTransferHandler(new TransferHandler() {
            @Override
            public boolean canImport(TransferSupport support) {
                return support.isDataFlavorSupported(DataFlavor.javaFileListFlavor);
            }

            @Override
            public boolean importData(TransferSupport support) {
                if (!canImport(support)) {
                    return false;
                }

                try {
                    final var fileList = (List<File>) support.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
                    if (fileList.isEmpty()) {
                        return false;
                    }
                    openFile(fileList.get(0));
                    return true;
                } catch (UnsupportedFlavorException e) {
                    JOptionPane.showMessageDialog(
                            MainAppWindow.this,
                            "Unsupported flavor: " + e.getMessage(),
                            "Platform error",
                            JOptionPane.ERROR_MESSAGE);
                } catch (IOException e) {
                    JOptionPane.showMessageDialog(
                            MainAppWindow.this,
                            "Could not fetch dragged file: " + e.getMessage(),
                            "I/O Error",
                            JOptionPane.ERROR_MESSAGE);
                }

                return false;
            }
        });

        setMode(ImageMode.INPUT);
        histogramViewer.setOriginalHistogramSupplier(() -> (engine == null) ? null : engine.originalHistogram());
        histogramViewer.setHistogramSupplier(() -> (engine == null) ? null : engine.histogram());
        histogramViewer.setThresholdsSupplier(() -> (engine == null) ? null : engine.thresholds());
        histogramViewer.addThesholdChangeListener(() -> {
            if (engine != null) {
                engine.invalidateOutput();
                repaint();
            }
        });

        imageViewer.setImageSupplier(this::getCurrentImage);

        visualizationPane.setDividerLocation(getHeight() - HISTOGRAM_HEIGHT);
        toolbarAndVisualizationPane.setDividerLocation(TOOLBAR_WIDTH);
    }

    /**
     * Entry point.
     */
    public static void main(String[] args) {
        new MainAppWindow().setVisible(true);
    }

    public synchronized void setMode(ImageMode mode) {
        Objects.requireNonNull(mode, "Mode cannot be null");
        this.mode = mode;
        if (engine == null) {
            return;
        }

        engine.invalidateOutput();
        imageModeGroup.setSelected(imageModeButtonMap.get(mode).getModel(), true);
        repaint();
    }

    private synchronized void setMeltingDirection(MeltingDirection direction) {
        Objects.requireNonNull(mode, "Direction cannot be null");
        if (engine == null) {
            return;
        }

        engine.invalidateOutput();
        engine.setMeltingDirection(direction);
        meltingDirectionGroup.setSelected(meltingDirectionButtonMap.get(direction).getModel(), true);
        repaint();
    }

    private synchronized void defaultThresholds() {
        if (engine == null) {
            return;
        }

        engine.defaultThresholds();
        repaint();
    }

    private synchronized void autoThresholdsBimodal() {
        if (engine == null) {
            return;
        }

        engine.autoThresholdsBimodal();
        repaint();
    }

    private synchronized void autoThresholdsMultimodal() {
        if (engine == null) {
            return;
        }

        engine.autoThresholdsMultimodal();
        repaint();
    }

    private synchronized void regenerateHistogram() {
        if (engine == null) {
            return;
        }

        try {
            engine.regenerateHistogram();
            repaint();
        } catch (ImageProcessingException e) {
            handleImageProcessingException(e);
        }
    }

    private synchronized void linearFilterHistogram() {
        if (engine == null) {
            return;
        }

        engine.linearFilterHistogram();
        repaint();
    }

    private synchronized void dilationFilterHistogram() {
        if (engine == null) {
            return;
        }

        engine.dilationFilterHistogram();
        repaint();
    }

    private synchronized void erosionFilterHistogram() {
        if (engine == null) {
            return;
        }

        engine.erosionFilterHistogram();
        repaint();
    }

    private synchronized void onOpenImageClicked() {
        final var result = openImageChooser.showOpenDialog(this);
        if (result != JFileChooser.APPROVE_OPTION) {
            return;
        }

        openFile(openImageChooser.getSelectedFile());
    }

    private synchronized void openFile(File file) {
        try {
            if (!file.exists()) {
                JOptionPane.showMessageDialog(
                        this,
                        "File '" + file.getName() + "' does not exist!",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }

            engine = ImageEditorEngine.load(file);
            setTitle(PROGRAM_TITLE + " \u2013 " + file.getName());
            updateControls();
            repaint();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(
                    this,
                    "Could not read file: " + e.getMessage(),
                    "I/O Error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (ImageProcessingException e) {
            handleImageProcessingException(e);
        }
    }

    private synchronized BufferedImage getCurrentImage() throws ImageProcessingException {
        if (engine == null) {
            return null;
        }

        return mode.imageFunction.apply(engine);
    }

    private synchronized void onSaveImageClicked() {
        try {
            final BufferedImage image = getCurrentImage();
            if (image == null) {
                JOptionPane.showMessageDialog(this, "There is no image to save.", "No image", JOptionPane.PLAIN_MESSAGE);
                return;
            }

            final var originalFile = openImageChooser.getSelectedFile();
            if (originalFile != null) {
                final var originalPath = originalFile.getAbsolutePath();
                int lastPoint = originalPath.lastIndexOf('.');
                saveImageChooser.setSelectedFile(new File(String.format("%s.out.%s",
                        originalPath.substring(0, lastPoint),
                        "bmp")));
            }

            final var result = saveImageChooser.showSaveDialog(this);
            if (result != JFileChooser.APPROVE_OPTION) {
                return;
            }

            ImageIO.write(image, "BMP", saveImageChooser.getSelectedFile());
        } catch (IOException e) {
            JOptionPane.showMessageDialog(
                    this,
                    "Could not write file: " + e.getMessage(),
                    "I/O Error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (ImageProcessingException e) {
            handleImageProcessingException(e);
        }
    }

    private synchronized void onCommitClicked() {
        if (engine == null) {
            return;
        }

        try {
            engine.commit();
            updateControls();
            repaint();
        } catch (ImageProcessingException e) {
            handleImageProcessingException(e);
        }
    }

    private synchronized void onUndoClicked() {
        if (engine == null) {
            return;
        }

        engine.undo();
        updateControls();
        repaint();
    }

    private synchronized void setIncludeMax(boolean includeMax) {
        if (engine == null) {
            return;
        }

        engine.setIncludeMax(includeMax);
    }

    private synchronized void handleImageProcessingException(ImageProcessingException e) {
        e.printStackTrace();
        JOptionPane.showMessageDialog(
                this,
                e.getMessage(),
                "Image processing error",
                JOptionPane.ERROR_MESSAGE);
    }

    private synchronized void updateControls() {
        if (engine == null) {
            return;
        }

        cooccurrenceIterationsModel.setValue(engine.getCooccurrenceIterations());
        autoThresholdsDepthModel.setValue(engine.getAutoThresholdDepth());
        meltingStartModel.setValue(engine.getMeltingStart());
        meltingStepsModel.setValue(engine.getMeltingSteps());
        includeMaxModel.setSelected(engine.getIncludeMax());
        setMode(engine.lastImageMode());
        setMeltingDirection(engine.getMeltingDirection());
    }
}
