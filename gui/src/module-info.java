module segmentation.gui {
    requires java.desktop;

    requires segmentation.api;
    uses cz.spiffyk.segmentation.ImageProcessingService;
}
