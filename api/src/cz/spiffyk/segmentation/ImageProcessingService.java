package cz.spiffyk.segmentation;

import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.Set;

public interface ImageProcessingService {

    /**
     * Performs checks on whether the specified image is supported and if it is not, throws an exception. The thrown
     * exception's message describes the problem with the image.
     *
     * @param image
     *      the image to check
     *
     * @throws ImageProcessingException
     *      when the image is not supported
     */
    void checkSupport(BufferedImage image) throws ImageProcessingException;

    /**
     * Calculates an absolute value histogram of the specified input image.
     *
     * @param image
     *      the input image
     *
     * @return the absolute value histogram
     *
     * @throws ImageProcessingException
     *      when the image is not supported or when the maximum value is exceeded
     */
    long[] histogram(BufferedImage image) throws ImageProcessingException;

    /**
     * Filters the provided histogram using the averaging FIR filter.
     *
     * @param histogram
     *      the input histogram
     * @param radius
     *      the radius of the filter, i.e. the number of values from left and right to use for filtering
     *
     * @return the filtered histogram
     */
    long[] linearFilterHistogram(long[] histogram, int radius);

    /**
     * Filters the provided histogram using dilation filter.
     *
     * @param histogram
     *      the input histogram
     * @param radius
     *      the radius of the filter, i.e. the number of values from left and right to use for filtering
     *
     * @return the filtered histogram
     */
    long[] dilationFilterHistogram(long[] histogram, int radius);

    /**
     * Filters the provided histogram using erosion filter.
     *
     * @param histogram
     *      the input histogram
     * @param radius
     *      the radius of the filter, i.e. the number of values from left and right to use for filtering
     *
     * @return the filtered histogram
     */
    long[] erosionFilterHistogram(long[] histogram, int radius);

    /**
     * Automatically generats thresholds based on a bimodal histogram.
     *
     * @param histogram
     *      the histogram to prcess
     * @param includeMax
     *      whether the found maximums should be included as thresholds
     *
     * @return a collection of thresholds
     */
    Collection<Integer> bimodalAutoThreshold(long[] histogram, boolean includeMax);

    /**
     * Automatically generates thresholds based on a multi-modal histogram.
     *
     * @param histogram
     *      the histogram to process
     * @param depth
     *      the depth of recursion
     *
     * @return a collection of thresholds
     */
    Collection<Integer> multimodalAutoThreshold(long[] histogram, int depth);

    /**
     * Calculates a thresholded image from the specified input image, using the specified thresholds.
     *
     * @param image
     *      the input image
     * @param thresholds
     *      the set of thresholds
     *
     * @return the thresholded image, may be {@code null} if there are less than two thresholds
     *
     * @throws ImageProcessingException
     *      when the image is not supported
     */
    BufferedImage threshold(BufferedImage image, Set<Integer> thresholds) throws ImageProcessingException;

    /**
     * Segments the specified image using its adjacency matrix.
     *
     * @param image
     *      the input image
     * @param iterations
     *      the number of iterations
     *
     * @return the segmented image
     *
     * @throws ImageProcessingException
     *      when the image is not supported
     */
    BufferedImage cooccurrence(BufferedImage image, int iterations) throws ImageProcessingException;

    /**
     * Segments the specified image using the brightness melting method.
     *
     * @param image
     *      the melting method
     * @param startBrightness
     *      the starting brightness, whose neighbour brightnesses are to be recolored
     * @param steps
     *      the number of steps of brightness melting
     * @param direction
     *      the direction of melting
     *
     * @return the segmented image
     *
     * @throws ImageProcessingException
     *      when the image, the number of steps, or the starting brightness is not supported
     */
    BufferedImage melt(BufferedImage image, int startBrightness, int steps, MeltingDirection direction) throws ImageProcessingException;

}
