package cz.spiffyk.segmentation;

public enum MeltingDirection {
    BIDIRECTIONAL, BRIGHTER_TO_DARKER, DARKER_TO_BRIGHTER
}
