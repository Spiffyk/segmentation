module segmentation.api {
    requires transitive java.desktop;

    exports cz.spiffyk.segmentation;
}
