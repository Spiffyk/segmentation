import cz.spiffyk.segmentation.ImageProcessingService;
import cz.spiffyk.segmentation.impl.ImageProcessingServiceImpl;

module segmentation.impl {
    requires segmentation.api;

    provides ImageProcessingService with ImageProcessingServiceImpl;
}
