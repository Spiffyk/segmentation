package cz.spiffyk.segmentation.impl;

import cz.spiffyk.segmentation.ImageProcessingException;
import cz.spiffyk.segmentation.ImageProcessingService;
import cz.spiffyk.segmentation.MeltingDirection;
import cz.spiffyk.segmentation.impl.util.IntBiPredicate;
import cz.spiffyk.segmentation.impl.util.LongBiPredicate;

import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.util.*;
import java.util.function.IntPredicate;

public class ImageProcessingServiceImpl implements ImageProcessingService {

    private static final int MAX_COLORS = 256;

    @Override
    public void checkSupport(BufferedImage image) throws ImageProcessingException {
        if (image.getType() != BufferedImage.TYPE_BYTE_GRAY) {
            throw new ImageProcessingException("Unsupported image type: Only grayscale images are supported!");
        }
    }

    @Override
    public long[] histogram(BufferedImage image) throws ImageProcessingException {
        checkSupport(image);

        final long[] result = new long[MAX_COLORS];
        final var raster = image.getData();
        for (int y = 0; y < raster.getHeight(); y++) {
            for (int x = 0; x < raster.getWidth(); x++) {
                final var i = raster.getSample(x, y, 0);
                if (result[i] == Long.MAX_VALUE) {
                    throw new ImageProcessingException("Maximum histogram value exceeded!");
                }
                result[i]++;
            }
        }
        return result;
    }

    @Override
    public long[] linearFilterHistogram(long[] histogram, int radius) {
        if (radius < 1) {
            throw new IllegalArgumentException("The radius may not be <1");
        }

        long[] result = new long[histogram.length];
        for (int i = 1; i < histogram.length - 1; i++) {
            double sum = (double) get(histogram, i);
            for (int offset = -radius; offset <= radius; offset++) {
                sum += (double) get(histogram, i + offset);
            }
            final int count = 2 * (radius + 1);
            result[i] = Math.round(sum / count);
        }
        return result;
    }

    @Override
    public long[] dilationFilterHistogram(long[] histogram, int radius) {
        if (radius < 1) {
            throw new IllegalArgumentException("The radius may not be <1");
        }

        long[] result = new long[histogram.length];
        for (int i = 1; i < histogram.length - 2; i++) {
            long max = Long.MIN_VALUE;
            for (int offset = -radius; offset <= radius; offset++) {
                final long curr = get(histogram, i + offset);
                if (curr > max) {
                    max = curr;
                }
            }

            result[i] = max;
        }
        return result;
    }

    @Override
    public long[] erosionFilterHistogram(long[] histogram, int radius) {
        if (radius < 1) {
            throw new IllegalArgumentException("The radius may not be <1");
        }

        long[] result = new long[histogram.length];
        for (int i = 1; i < histogram.length - 2; i++) {
            long min = Long.MAX_VALUE;
            for (int offset = -radius; offset <= radius; offset++) {
                final long curr = get(histogram, i + offset);
                if (curr < min) {
                    min = curr;
                }
            }

            result[i] = min;
        }
        return result;
    }

    @Override
    public Collection<Integer> bimodalAutoThreshold(long[] histogram, boolean includeMax) {
        final Set<Integer> result = new HashSet<>();

        int leftMax = nextLocalMax(histogram, 0, histogram.length);
        int rightMax = nextLocalMax(histogram, histogram.length - 1, -1);

        if (includeMax) {
            result.add(leftMax);
            result.add(rightMax);
        }

        findCenterMinimums(result, histogram, 0, leftMax, rightMax);

        return result;
    }

    @Override
    public Collection<Integer> multimodalAutoThreshold(long[] histogram, int depth) {
        final Set<Integer> result = new HashSet<>();

        int leftMax = nextLocalMax(histogram, 0, histogram.length);
        int leftMin = nextLocalMin(histogram, leftMax, histogram.length);
        int rightMax = nextLocalMax(histogram, histogram.length - 1, -1);
        int rightMin = nextLocalMin(histogram, rightMax, -1);

        result.add(leftMin);
        result.add(rightMin);

        if (depth >= 1) {
            findCenterMinimums(result, histogram, depth - 1, leftMin, rightMin);
        }

        return result;
    }

    private void findCenterMinimums(Set<Integer> result, long[] histogram, int depth, int left, int right) {
        final int center = (left + right) / 2;

        int centerMin;
        final boolean added;
        if (histogram[center + 1] < histogram[center]) {
            // go right
            centerMin = nextLocalMin(histogram, center + 1, right);
            added = result.add(centerMin);
        } else if (histogram[center + 1] > histogram[center]) {
            // go left
            centerMin = nextLocalMin(histogram, center, left);
            added = result.add(centerMin);
        } else {
            centerMin = center;
            added = true;
        }

        if (added && depth >= 1) {
            findCenterMinimums(result, histogram, depth - 1, left, centerMin);
            findCenterMinimums(result, histogram, depth - 1, centerMin, right);
        }
    }

    private int nextLocalMin(long[] in, int startPosition, int endPosition) {
        return nextLocalExtreme(in, startPosition, endPosition, (curr, last) -> (curr > last));
    }

    private int nextLocalMax(long[] in, int startPosition, int endPosition) {
        return nextLocalExtreme(in, startPosition, endPosition, (curr, last) -> (curr < last));
    }

    private int nextLocalExtreme(long[] in, int startPosition, int endPosition, LongBiPredicate test) {
        final int step = (startPosition < endPosition) ? 1 : -1;
        final IntPredicate predicate = (startPosition < endPosition) ?
                (i -> (i < endPosition)) :
                (i -> (i > endPosition));

        long last = in[startPosition];
        for (int i = startPosition + step; predicate.test(i); i += step) {
            long curr = in[i];
            if (test.test(curr, last)) {
                return i - step;
            }
            last = curr;
        }

        return startPosition;
    }

    @Override
    public BufferedImage threshold(BufferedImage image, Set<Integer> inThresholds) throws ImageProcessingException {
        checkSupport(image);

        final var thresholds = new TreeSet<>(inThresholds);
        if (thresholds.isEmpty()) {
            return null;
        }

        final var result = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        final var inRaster = image.getData();
        final var outRaster = result.getRaster();
        for (int y = 0; y < inRaster.getHeight(); y++) {
            for (int x = 0; x < inRaster.getWidth(); x++) {
                final var i = inRaster.getSample(x, y, 0);
                var thresholded = thresholds.floor(i);
                if (thresholded == null) {
                    thresholded = 0;
                }
                outRaster.setSample(x, y, 0, thresholded);
            }
        }
        return result;
    }

    @Override
    public BufferedImage cooccurrence(BufferedImage image, int iterations) throws ImageProcessingException {
        checkSupport(image);

        if (iterations <= 0) {
            return image;
        }

        final var inRaster = image.getData();
        final var result = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        final var outRaster = result.getRaster();
        for (int y = 0; y < inRaster.getHeight(); y++) {
            for (int x = 0; x < inRaster.getWidth(); x++) {
                outRaster.setSample(x, y, 0, inRaster.getSample(x, y, 0));
            }
        }

        final long[][] matrix = new long[MAX_COLORS][MAX_COLORS];
        for (int iter = 0; iter < iterations; iter++) {
            cooccurrenceMatrix(matrix, outRaster);

            long min = max(matrix);
            int oldColor = -1;
            for (int i = 0; i < MAX_COLORS; i++) {
                final long m = matrix[i][i];
                if (m >= min || (m == 0 && sum(matrix[i]) == 0)) {
                    continue;
                }

                min = m;
                oldColor = i;
            }

            if (oldColor < 0) {
                return result;
            }

            long max = 0;
            int newColor = -1;
            for (int j = 0; j < MAX_COLORS; j++) {
                final long m = matrix[oldColor][j];
                if (m > max && j != oldColor) {
                    max = m;
                    newColor = j;
                }
            }

            if (newColor < 0) {
                return result;
            }

            for (int y = 0; y < outRaster.getHeight(); y++) {
                for (int x = 0; x < outRaster.getWidth(); x++) {
                    final int color = outRaster.getSample(x, y, 0);
                    if (color == oldColor) {
                        outRaster.setSample(x, y, 0, newColor);
                        assert outRaster.getSample(x, y, 0) == newColor;
                    }
                }
            }
        }

        return result;
    }

    @Override
    public BufferedImage melt(BufferedImage image, int startBrightness, int steps, MeltingDirection direction) throws ImageProcessingException {
        checkSupport(image);
        if (startBrightness < 0 || startBrightness > 255) {
            throw new ImageProcessingException("Starting brightness of " + startBrightness + " is not in the interval <0, 255>");
        }

        final IntBiPredicate directionPredicate = resolveMeltingPredicate(direction);

        final var inRaster = image.getData();
        final var result = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        final var outRaster = result.getRaster();
        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                final var inColor = inRaster.getSample(x, y, 0);
                if (Math.abs(inColor - startBrightness) <= steps && directionPredicate.test(inColor, startBrightness)) {
                    outRaster.setSample(x, y, 0, startBrightness);
                } else {
                    outRaster.setSample(x, y, 0, inColor);
                }
            }
        }

        return result;
    }

    private IntBiPredicate resolveMeltingPredicate(MeltingDirection direction) {
        switch (direction) {
            case BIDIRECTIONAL:
                return (inColor, startBrightness) -> true;
            case DARKER_TO_BRIGHTER:
                return (inColor, startBrightness) -> (inColor - startBrightness) <= 0;
            case BRIGHTER_TO_DARKER:
                return (inColor, startBrightness) -> (inColor - startBrightness) >= 0;
            default:
                throw new IllegalArgumentException("Unsupported direction " + direction);
        }
    }

    private void cooccurrenceMatrix(long[][] matrix, Raster raster) {
        for (long[] line : matrix) {
            Arrays.fill(line, 0);
        }

        for (int y = 1; y < raster.getHeight() - 1; y++) {
            for (int x = 1; x < raster.getWidth() - 1; x++) {
                int i = raster.getSample(x, y, 0);
                incMatrix(matrix, i, raster.getSample(x - 1, y - 1, 0));
                incMatrix(matrix, i, raster.getSample(x - 1, y, 0));
                incMatrix(matrix, i, raster.getSample(x - 1, y + 1, 0));
                incMatrix(matrix, i, raster.getSample(x, y + 1, 0));
                incMatrix(matrix, i, raster.getSample(x + 1, y + 1, 0));
                incMatrix(matrix, i, raster.getSample(x + 1, y, 0));
                incMatrix(matrix, i, raster.getSample(x + 1, y - 1, 0));
                incMatrix(matrix, i, raster.getSample(x, y - 1, 0));
            }
        }
    }

    private void incMatrix(long[][] matrix, int i, int j) {
        matrix[i][j]++;
        if (i != j) {
            matrix[j][i]++;
        }
    }

    private long max(long[][] matrix) {
        long max = Long.MIN_VALUE;
        for (long[] line : matrix) {
            for (long num : line) {
                if (num > max) {
                    max = num;
                }
            }
        }
        return max;
    }

    private long sum(long[] line) {
        return Arrays.stream(line).sum();
    }

    private long get(long[] array, int i) {
        if (i < 0) {
            return array[0];
        } else if (i >= array.length) {
            return array[array.length - 1];
        } else {
            return array[i];
        }
    }

}
