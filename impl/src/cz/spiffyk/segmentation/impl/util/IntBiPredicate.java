package cz.spiffyk.segmentation.impl.util;

@FunctionalInterface
public interface IntBiPredicate {
    boolean test(int i, int j);
}
