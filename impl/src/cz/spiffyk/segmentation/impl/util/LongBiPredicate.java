package cz.spiffyk.segmentation.impl.util;

@FunctionalInterface
public interface LongBiPredicate {
    boolean test(long i, long j);
}
